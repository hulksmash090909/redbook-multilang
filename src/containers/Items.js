import React from 'react'
import OneItem from './OneItem.js'

const Items = ({props}) => {
    console.log('react == ', props)
    return(
        <div className="myScrollBar">
            <div>
                <ul className="list-unstyled">
                    {props.items.map((item, index) => {
                        console.log('index =', index)
                        return (
                            <OneItem item={item} index={index} props={props} key={index} />
                        )
                    })
                    }
                </ul>
            </div>
        </div>
    )
}

export default Items
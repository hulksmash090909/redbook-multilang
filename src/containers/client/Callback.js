import React from 'react'
import * as action from '../../store/action'

const closer = (props) => {
    // props.dispatch( action.alert(null, null) )
    document.querySelector("div#notification").className += ' displayNone';
}

const submit = (e, props) => {
    e.preventDefault()
    let sms = document.querySelector("#sms1").value
    let name = document.querySelector("#uname1").value
    props.dispatch( action.setCallback(sms, name) )
    props.dispatch( action.getCallbacks() )
}

const Callback = ({props}) => {
    return(
        <div className="history">
        


            <div className="card card-body jumbotron center-a-i">
                <div id="scrollingChat" className="scrollingChat">
                    <div className="chat">
                        { props.items.map((callback, index) => {
                                                                    
                            return (
                                <div>
                                    <span className="input-group-text" id="basic-addon3"><strong >{callback.name}</strong></span>
                                    <div key={index} className="mine messages">                                        
                                        <div className="message last">
                                            {callback.callback}
                                        </div>            
                                    </div>
                                </div>)
                        }) }   
                    </div> 
                </div>
                <form onSubmit={(e)=>{submit(e, props)}}>
                    <div className="input-group mb-3">
                        <div><input id="uname1" type="text" className="form-control" placeholder="Ваше имя" required/></div>
                        <div><input id="sms1" type="text" className="form-control" placeholder="Отзыв" aria-label="Recipient's username" aria-describedby="button-addon2" required/></div>
                        <div className="input-group-append">
                            <button type="submit" className="btn btn-outline-secondary zindex">Отправить</button>
                        </div>
                    </div> 
                </form>

            </div>
        </div> 
    )
}

export default Callback

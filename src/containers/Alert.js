import React from 'react'
import * as action from '../store/action'

const closer = (props) => {
    // props.dispatch( action.alert(null, null) )
    document.querySelector("div#notification").className += ' displayNone';
}

const Alert = ({props}) => {
    return(
        <div id="notification" className={'alert alert-' + props.alertType + ' alert-dismissible fade show displayNone'} role="alert">
            {props.alertMes}
            <button onClick={() => {closer(props)}} id="notificationClose" type="button" className="close" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div> 
    )
}

export default Alert

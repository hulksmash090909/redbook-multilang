import React from 'react'
import * as action from '../store/action'

const audioPlay = () => {
    let audio = new Audio('https://firebasestorage.googleapis.com/v0/b/nodeproject-1d5eb.appspot.com/o/RedBook%2Faudio%2Fanimals%2Fcherepashki-nindzya-2014-a_rival---tmnt-2012-tm.mp3?alt=media&token=88c9cfe1-56c3-4a11-9a41-e46ee48d6626')
    audio.play()
}

const save = (url) => {
    const xhr = new XMLHttpRequest();
    xhr.responseType = 'blob';
    xhr.onload = function(event) {
        let blob = xhr.response;
        let blobURL = URL.createObjectURL(blob)
        document.querySelector("#toSave").href = blobURL
        document.querySelector("#toSave").click()
    };
    xhr.open('GET', url);
    xhr.send();
}
const chooseLang = (props, lang, item, key) => {
    let langAudio = null
    let myAudio = document.querySelector("audio#audioTag" + key)
    if (lang === 'rus') {
        langAudio = item.rusAudio
    } else if (lang === 'krch') {
        langAudio = item.krchAudio
    } else if (lang === 'chrk') {
        langAudio = item.chrkAudio
    } else if (lang === 'uzb') {
        langAudio = item.uzbAudio
    }
    if (langAudio) {
        myAudio.src = langAudio
    } else {
        // props.dispatch( action.alert( 'Нет озвучки на данном языке', 'danger' ) )
        document.querySelector("div#notification").classList.remove('displayNone');
        myAudio.src = ''
    }
}

const OneItem = ({item, index, props}) => {
    console.log('react22 == ', item)  
    console.log('key222 == ', index)  
    return (<li className="media itemsColumn pt-20">
    <div className="badge badge-primary text-wrap">
        
        {///////TEXT
            item.name ? (
            <div>
                <button className="btn btn-light" type="button" data-toggle="collapse" data-target={"#collapseExample" + index} aria-expanded="false" aria-controls={"collapseExample" + index}>
                    {item.name}
                </button>
                <div className="collapse" id={"collapseExample" + index}>
                    {item.nod ? (
                        <div className="card card-body black" dangerouslySetInnerHTML={ {__html: item.nod} }></div>
                        ) : 
                    (
                    <div>
                        <div className="card card-body black" dangerouslySetInnerHTML={ {__html: item.text} }></div>
                        <div className="accordion" id={"accordionExample" + index}>
                            <div className="card">
                                <div className="card-header" id="headingOne">
                                <h2 className="mb-0">
                                    <button className="btn btn-link" type="button" data-toggle="collapse" data-target={"#collapseOne" + index} aria-expanded="true" aria-controls={"collapseOne" + index}>
                                    Кроссворд
                                    </button>
                                </h2>
                                </div>

                                <div id={"collapseOne" + index} className="collapse show" aria-labelledby="headingOne" data-parent={"#accordionExample" + index}>
                                    <div className="card-body">
                                        <img className="my-imgs" src={item.crosIMG} alt="Пожалуйста подождите"/>
                                    </div>
                                </div>
                            </div>
                            <div className="card">
                                <div className="card-header" id="headingTwo">
                                <h2 className="mb-0">
                                    <button className="btn btn-link collapsed" type="button" data-toggle="collapse" data-target={"#collapseTwo" + index} aria-expanded="false" aria-controls={"collapseTwo" + index}>
                                    Подсмотреть ответы
                                    </button>
                                </h2>
                                </div>
                                <div id={"collapseTwo" + index} className="collapse" aria-labelledby="headingTwo" data-parent={"#accordionExample" + index}>
                                <div className="card-body">
                                    <img className="my-imgs" src={item.ansCrosIMG} alt="Пожалуйста подождите"/>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>)}


                </div>
            </div>) :
            ////////////IMAGES
            (<div>
                <div className="badge badge-primary text-wrap">
                    <h5 className="mt-0 mb-1">{item.rusName} - {item.krchName}{item.chrkName}{item.natName}</h5>   
                </div>
                <img src={item.imgURL} className="mr-3 listSize" alt="..."/>
                <div className="media-body">
                    <div>
                        <p>
                            {props.history.location.pathname === '/items' ? 
                                (<button className="btn btn-light" type="button" data-toggle="collapse" data-target={"#collapseExample" + index} aria-expanded="false" aria-controls="collapseExample">
                                    Прослушать название
                                </button>) : 
                            (<button onClick={() => {save(item.imgURL)}} className="btn btn-light" type="button">
                                Скачать раскраску
                            </button>)}
                        </p>
                        <div className="collapse" id={"collapseExample" + index}>
                            {item.audioURL ? 
                            (<div>
                                <audio id={'audioTag' + index} controls src={ item.audioURL}></audio>
                            </div>) :
                            (<div className="alert alert-danger" role="alert">
                                У данного животного нет озвучки
                            </div>)}
                        </div>
                    </div>
                    <a id="toSave" href="#" download="Coloring.jpg"></a>


                        {/* <div>
                            <button onClick={() => {audioPlay()}} className="btn btn-outline-dark">на русском</button>
                            <button onClick={() => {audioPlay()}} className="btn btn-outline-dark">на черкесском</button>   
                        </div>
                        <select onChange={(e) => {chooseLang(props, e.target.value, item, index)}} className="custom-select" id="inputGroupSelect02">
                            <option defaultValue>Выберите язык озвучки</option>
                            <option value="rus">русский</option>
                            <option value="krch">карачаевский</option>
                            <option value="chrk">черкесский</option>
                            <option value="uzb">ногайский</option>
                        </select> */}
                </div>
            </div>)
        }
    </div>
    </li>)
}

export default OneItem
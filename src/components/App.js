import React from 'react'
import { BrowserRouter } from "react-router-dom"
import PageMyRouter from './MyRouter.js'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';
import '../App.css'
import '../customCSS/customCSS.css'
import '../customCSS/robotoCSS.css'

class App extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      a: 1,
   };
 }

 render() {
   console.log('AAA = ', this.props)
   //this.state.history.push('/aaaaa')
  return (
    <div className="App">
      <div className="container">
        <BrowserRouter>
          <PageMyRouter />
        </BrowserRouter>
      </div>
    </div>
  );
 }
}

export default App;

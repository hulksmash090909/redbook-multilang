import React from 'react'
import { Route, withRouter } from "react-router-dom"
import { connect } from 'react-redux'
//import SuperAdmin from '../containers/Page404.js'
import Header from '../blocks/Header.js'
import Footer from '../blocks/Footer.js'
import Items from '../containers/Items.js'
import OneItem from '../containers/client/OneItem.js'
import Colors from '../containers/client/Colors.js'
import Callback from '../containers/client/Callback.js'
import Page404 from '../containers/Page404.js'
import * as action from '../store/action'

class MyRouter extends React.Component {

    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        const WrappedItems = () => {
            return (<Items props={this.props} />);
        };
        const WrappedCallbacks = () => {
            return (<Callback props={this.props} />);
        };
      
        return (
            <page-MyRouter>
                <div className="to-wright-window">
                    <Header props={this.props}/>
                    <div className="to-all-window">
                        <Route exact path="/items" component={WrappedItems} />
                        <Route path="/createdHistory" component={OneItem} />
                        <Route path="/coloursMain" component={Colors} />
                        <Route path="/teachers" component={WrappedItems} />
                        <Route path="/parents" component={WrappedItems} />
                        <Route path="/kids" component={WrappedItems} />
                        <Route path="/callbacks" component={WrappedCallbacks} />
                        <Route path="/notFound" component={Page404} />
                    </div>
                    <Footer/>
                </div>
            </page-MyRouter>
        );
    }
}

const mapStateToProps = state => ({
    containerPath: state.rootReducer.containerPath,
    items: state.rootReducer.items,
    alertType: state.rootReducer.alertType,
    alertMes: state.rootReducer.alertMes,
    langPath: state.rootReducer.langPath,
})

const PageMyRouter = connect(mapStateToProps)(MyRouter)
export default withRouter(PageMyRouter);

import { combineReducers } from 'redux'

const MY_STATE = {
    containerPath: null,
    items: [],
    alertType: 'danger',
    alertMes: 'Нет озвучки на данном языке',
    langPath: 'null',
}

export const rootReducer = (state = MY_STATE, action) => {
    switch(action.type) {
        case 'CONTAINER_PATH':
            console.log('dd2')
            return {
                ...state,
                containerPath: action.payload,
            }
        case 'GET_ITEMS':
        console.log('dd33')
            return {
                ...state,
                items: action.payload,
                langPath: action.langPath,
            }
        case 'ALERT_DANGER':
        console.log('danger')
            return {
                ...state,
                alertMes: action.payload,
                alertType: 'danger',
            }
        case 'ALERT_SUCCESS':
        console.log('ALERT_SUCCESS')
            return {
                ...state,
                alertMes: action.payload,
                alertType: 'success',
            }
        case 'GET_COLORINGS':
            return {
                ...state,
                items: action.payload,
            }
        case 'GET_CROSSWORDS':
            return {
                ...state,
                items: action.payload,
            }
        case 'GET_NODS':
            return {
                ...state,
                items: action.payload,
            }
        case 'GET_CALLBACKS':
            return {
                ...state,
                items: action.payload,
            }
        default:
                return state
    }
}

export default combineReducers({
    rootReducer
})
  
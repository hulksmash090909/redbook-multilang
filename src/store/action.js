import firebase, {db} from "../components/Firebase.js"

export const containerPath = (containerPath) => {
    console.log('dddd')
    return function action(dispatch) {
        dispatch({type: 'CONTAINER_PATH', payload: containerPath})
        return "CONTAINER_PATH"
    }
}

export const getItems = (path) => {
    console.log('get333', path)
    return function action(dispatch) {
        let items = []
        db.collection(path).get().then((querySnapshot) => {
            console.log('dbdbdbd == ', querySnapshot)
            querySnapshot.forEach((doc) => {
                items.push(doc.data());
            });
            dispatch({type: 'GET_ITEMS', payload: items, langPath: path})
            console.log('ACTIONSS = ', items)
            return "CONTAINER_PATH"
        });
    }
}

export const alert = (mes, type) => {
    console.log('ALERT3')
    return function action(dispatch) {
        if (type === 'danger') {
            dispatch({type: 'ALERT_DANGER', payload: mes})
            return "ALERT_DANGER"
        } else {
            dispatch({type: 'ALERT_SUCCESS', payload: mes})
            return "ALERT_SUCCESS"
        }
    }
}

export const getColorings = () => {
    return function action(dispatch) {
        let items = []
        db.collection('colorings').get().then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
                items.push(doc.data());
            });
            dispatch({type: 'GET_COLORINGS', payload: items})
            return "GET_COLORINGS"
        });
    }
}

export const getCrosswords = () => {
    return function action(dispatch) {
        let items = []
        db.collection('parents').get().then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
                items.push(doc.data());
            });
            dispatch({type: 'GET_CROSSWORDS', payload: items})
            return "GET_CROSSWORDS"
        });
    }
}

export const getNods = () => {
    return function action(dispatch) {
        let items = []
        db.collection('nod').get().then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
                items.push(doc.data());
            });
            dispatch({type: 'GET_NODS', payload: items})
            return "GET_NODS"
        });
    }
}

export const getCallbacks = () => {
    return function action(dispatch) {
        let items = []
        db.collection('callbacks').get().then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
                items.push(doc.data());
            });
            dispatch({type: 'GET_CALLBACKS', payload: items})
            return "GET_CALLBACKS"
        });
    }
}

export const setCallback = (sms, name) => {
    return function action(dispatch) {
        db.collection("callbacks").add({
            name: name,
            callback: sms,
            date: Date(),
        })
        .then(function(docRef) {
            console.log("Document written with ID: ", docRef.id);
            //dispatch({type: 'GET_CALLBACKS', payload: items})
        })
        .catch(function(error) {
            console.error("Error adding document: ", error);
        });
        return "URAA"
    }
}
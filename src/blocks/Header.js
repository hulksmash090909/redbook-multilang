import React from 'react'
import { connect } from 'react-redux'
import * as action from '../store/action'
import Alert from '../containers/Alert.js'

const containerPath = (props, path) => {
    //props.history.push(path)
    if (path === 'chrk') {
        path = 'animals'
    }
    props.dispatch( action.containerPath( path ) )
    let containerPath = path
    if (containerPath) {
        //let path2 = containerPath.substring(1)
        console.log('componentWillMount = ', props)
        props.dispatch( action.getItems( path ) )
    }
}

const mainNavigation = (props, path) => {
    props.history.push(path)
    if (path==='/kids') {
        props.dispatch( action.getColorings() )
    } else if (path==='/parents') {
        props.dispatch( action.getCrosswords() )
    } else if (path==='/items') {
        props.dispatch( action.getItems('null') )
    } else if (path==='/teachers') {
        props.dispatch( action.getNods() )
    } else if (path==='/callbacks') {
        props.dispatch( action.getCallbacks() )
    }
}

const Header = ({props}) => {
    console.log('propsHeader', props)
    console.log('propsHeader2222', props.containerPath)
    //console.log('propsHeader2222', props.rootReducer.containerPath)
    return (
        <div className="container myNavbarHeader">
            <nav className="navbar navbar-dark bg-primary myNav">
                <a className="navbar-brand" href="#">Красная книга</a>
                { props.history.location.pathname === '/items'
                    ? (<div>
                        <select onChange={(e) => containerPath(props, e.target.value)} className="custom-select" id="inputGroupSelect02">
                            <option defaultValue>Выберите язык озвучки</option>
                            <option value="krch">Карачаевский</option>
                            <option value="chrk">Черкесский</option>
                            <option value="abz">Абазинский</option>
                            <option value="ngs">Ногайский</option>
                        </select></div>
                        )
                    : '' }
                <div className="dropdown">
                    <div className="btn-group dropleft">
                        <button className="bg-primary" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div className="dropdown_threads"></div>
                            <div className="dropdown_threads"></div>
                            <div className="dropdown_threads"></div>
                        </button>
                        <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <button className="dropdown-item" onClick={() => {mainNavigation(props, '/createdHistory')}}>История создания<br/> Красной книги</button><hr/>
                            <button className="dropdown-item" onClick={() => {mainNavigation(props, '/coloursMain')}}>Значение цветов <br/>Красной книги</button><hr/>
                            <button className="dropdown-item" onClick={() => {mainNavigation(props, '/teachers')}}>Страница педагога</button><hr/>
                            <button className="dropdown-item" onClick={() => {mainNavigation(props, '/parents')}}>Страница родителя</button><hr/>
                            <button className="dropdown-item" onClick={() => {mainNavigation(props, '/kids')}}>Страница дошкольника</button><hr/>
                            <button className="dropdown-item" onClick={() => {mainNavigation(props, '/items')}}>Животный и растительный<br/> мир планеты</button><hr/>
                            <button className="dropdown-item" onClick={() => {mainNavigation(props, '/callbacks')}}>Отзывы</button>
                        </div>
                    </div>
                </div>
            </nav>
            <Alert props={props} />
        </div>
    )
}

// const mapStateToProps = state => ({
//     containerPath: state.rootReducer.containerPath,
// })
//{ props.alertMes ? (<Alert props={props} />) : '' }
// export default connect(mapStateToProps)(Header)
export default Header
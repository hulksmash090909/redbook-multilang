import React from 'react'

const Footer = () => {
    return (
        <div className="container">
            <nav className="navbar navbar-dark bg-primary myNav">
                <img className="footer-img" src="https://firebasestorage.googleapis.com/v0/b/nodeproject-1d5eb.appspot.com/o/RedBook%2Fdownload.jpeg?alt=media&token=137dd567-cbcd-44df-b214-a74270c791b6" alt="Красная книга"></img>
            </nav>
        </div>
    )
}

export default Footer